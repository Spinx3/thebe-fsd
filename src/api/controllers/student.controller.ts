import { FindAttributeOptions } from "sequelize/types";
import sequelize from "../../config/sequelize.config";
import StudentModel from "../models/student.model";

/**
 * create new Student from info provided
 * @param info object containing Student data to be saved to DB
 * @returns 
 */
function createOne(info: Record<string, unknown>): Promise<unknown> {
    // Implement validation checks for all posted info
    return new Promise((resolve, reject) => {
        // use managed transaction to perform commit/rollback of query
        try {
            const result = sequelize.transaction(async (createUserTrans) => {
                //construct profile info
                const profileInfo = {
                    firstName: info.firstName,
                    lastName: info.lastName,
                    otherNames: info.otherNames || '',
                    emailAddress: info.emailAddress,
                    dateOfBirth: info.dateOfBirth
                };

                /*
                    1. Start new transaction to perform op
                    2. Read any record with email given and exit op if record found
                    3. Insert new record, commit and return newly created if record is not found
                */
                const [user, createdNow] = await StudentModel.findOrCreate({
                    where: { emailAddress: info.emailAddress },
                    defaults: profileInfo,
                    transaction: createUserTrans
                });

                // send error if the record wasn't newly created in this operation
                !createdNow && reject(new Error(`Error: User already exists. ID:${user.get('_id')}`));
            });

            resolve(result);
        } catch (error) {
            reject(error);
        }
    });
}

/**
 * Read atmost one record having primary key {id} and LIMIT operation to 1 read|first find
 * @param id the primary key|identifier of the record to match
 * @returns 
 */
async function getById(id: string, projection_columns: FindAttributeOptions | undefined): Promise<unknown> {
    return await StudentModel.findByPk(id, { attributes: projection_columns });
}

/**
 * Read all records matching any criteria if any or all
 * @param rows_matching specify records included in where clause
 * @param projection_columns columns to be return from dat table(s)
 * @returns 
 */
async function getAll(rows_matching: Record<string, unknown>, projection_columns: FindAttributeOptions | undefined): Promise<unknown> {
    // TODO implement row selection
    return await StudentModel.findAll({ attributes: projection_columns });
}

const studentController = { createOne, getById, getAll }

export default studentController;

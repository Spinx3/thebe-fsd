import { DataTypes } from "sequelize";
import sequelize from "../../config/sequelize.config";

const ClassModel = sequelize.define('Student', {
    _id: {
        type: DataTypes.UUID,
        allowNull: false,
        primaryKey: true,
        defaultValue: DataTypes.UUIDV4
    },
    name: {
        type: DataTypes.STRING,
        allowNull: false
    }
}, { tableName: 'class' });

export default ClassModel;

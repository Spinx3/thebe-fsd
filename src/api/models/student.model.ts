import { DataTypes } from "sequelize";
import sequelize from "../../config/sequelize.config";

const StudentModel = sequelize.define('Student', {
    _id: {
        type: DataTypes.UUID,
        allowNull: false,
        primaryKey: true,
        defaultValue: DataTypes.UUIDV4
    },
    firstName: {
        type: DataTypes.STRING,
        allowNull: false
    },
    lastName: {
        type: DataTypes.STRING,
        allowNull: false
    },
    otherNames: {
        type: DataTypes.STRING,
        allowNull: true
    },
    emailAddress: {
        type: DataTypes.STRING,
        allowNull: false,
        unique: true
    },
}, { tableName: 'students' });

export default StudentModel;


import { config } from "dotenv";
import { createServer, Server } from "http";
import ClassModel from "./api/models/class.model";
import StudentModel from "./api/models/student.model";
import TutorModel from "./api/models/tutor.model";

import appServer from './config/express.config';
import sequelize from "./config/sequelize.config"; "./config/sequelize.config";

// enable process.env to pick from local .env file
config();

// init http server app and configure express app as default request listener
const server: Server = createServer(appServer);
const port = process.env.APP_PORT;

function serve() {

    server.listen(port, () => {
        console.log(`listening on http://localhost:${port}.`);
    });

    server.on('listening', async () => {

        TutorModel.hasMany(ClassModel, {
            as: 'classes',
            foreignKey: 'tutorId',
            sourceKey: '_id',
            onDelete: 'cascade',
            onUpdate: 'cascade',
            constraints: true,
            hooks: true
        });

        StudentModel.hasMany(ClassModel, {
            as: 'classes',
            foreignKey: 'studentId',
            sourceKey: '_id',
            onDelete: 'cascade',
            onUpdate: 'cascade',
            constraints: true,
            hooks: true
        });

        // sync db creation
        await sequelize.sync()
            .then(() => { console.log('DB sync successful'); })
            .catch((error) => { console.log(`DB sync failed: ${error}`); });
    });

    server.on('error', (error1) => {
        console.error(error1);

        server.close((error2) => {
            console.error(error2);
        });
    });

    server.on('close', () => {
        sequelize.close();
        console.log('Server shutdown');
    });

}

// startup server
serve();
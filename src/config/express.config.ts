import express, { NextFunction, Request, Response } from "express";
import routeHandle from "../routes/index.route";
import tutorHandle from "../routes/tutor.route";
import studentHandle from "../routes/student.route";
import path from "path";

const appServer: express.Application = express();  // init express app -- request listener
const appRouter: express.Router = express.Router();  // init express router -- for route redirection

// config route listener
routeHandle(appRouter);  // handle all route access to /
tutorHandle(appRouter);  // handle all route access to /tutor
studentHandle(appRouter);  // handle all route access to /student

// config express middleware
appServer.set('view engine', 'pug');  // use pug as default view engine
appServer.set('views', path.join(__dirname, '..', 'api/views'));  // directory containing views

appServer.use(express.json({}));  // parse json encoded request body
appServer.use(express.urlencoded({ extended: false }));  // parse url encoded request body
appServer.use('/', appRouter);  // use appRouter on / request calls
appServer.use(errorHandler);  // config error handler

appServer.use(express.static(path.join(__dirname, '../../', 'public')));  // serve static files from public folder

function errorHandler(error: unknown, request: Request, response: Response, next: NextFunction) {
    console.error(error);
    response.status(404);

    next();
}

export default appServer;

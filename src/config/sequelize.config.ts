import path from "path";
import { Sequelize, Transaction } from "sequelize";
import * as appRootPath from "app-root-path";

const sequelize = new Sequelize({
    dialect: 'sqlite',
    storage: path.join(`${appRootPath}/data`, 'database.sqlite'),
    password: process.env.DB_PASS || '',
    username: process.env.DB_USER || '',
    transactionType: Transaction.TYPES.IMMEDIATE,
    retry: {
        max: 5
    }
});

export default sequelize;

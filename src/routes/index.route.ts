import { Router } from "express";

function routeHandle (router: Router): void {

    const routePath = '/';

    router.all(`${routePath}`, (request, response, next) => {
        response.render('index');
        next();
    });

}

export default routeHandle;
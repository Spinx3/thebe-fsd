import { NextFunction, Router, Request, Response } from "express";
import studentController from "../api/controllers/student.controller";

/**
 * Handler for all access to /student
 * @param router express Router instance
 */
function studentHandle(router: Router): void {

    const routePath = '/student';

    router.get(`${routePath}s`, async (request: Request, response: Response, next: NextFunction) => {
        const result = await studentController.getAll({}, undefined);
        response.json(result || false);
        next();
    });

    router.get(`${routePath}/:id`, async (request: Request, response: Response, next: NextFunction) => {
        const id = request.params.id;

        if (id === '' || id === undefined) {
            response.status(404);
        } else {
            const result = await studentController.getById(id, undefined);
            response.json(result || false);
        }

        next();
    });

    router.post(`${routePath}`, async (request: Request, response: Response, next: NextFunction) => {
        const result = await studentController.createOne(request.body);
        response.json(result || false);
        next();
    });

}

export default studentHandle;
import { NextFunction, Router, Request, Response } from "express";
import tutorController from "../api/controllers/tutor.controller";

/**
 * Handler for all access to /tutor
 * @param router express Router instance
 */
function tutorHandle(router: Router): void {

    const routePath = '/tutor';

    router.get(`${routePath}s`, async (request: Request, response: Response, next: NextFunction) => {
        const result = await tutorController.getAll({}, undefined);
        response.json(result || false);
        next();
    });

    router.get(`${routePath}/:id`, async (request: Request, response: Response, next: NextFunction) => {
        const id = request.params.id;

        if (id === '' || id === undefined) {
            response.status(404);
        } else {
            const result = await tutorController.getById(id, undefined);
            response.json(result || false);
        }

        next();
    });

    router.post(`${routePath}`, async (request: Request, response: Response, next: NextFunction) => {
        const result = await tutorController.createOne(request.body);
        response.json(result || false);
        next();
    });

}

export default tutorHandle;